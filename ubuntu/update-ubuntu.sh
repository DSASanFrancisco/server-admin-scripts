set -e

# reboots servers after 30 days of uptime
rebootUptime=30

echo ' '
echo -e "Starting OS Updates..."
echo ' '
sleep 5
sudo apt-get update -y
echo -e "Repository update finished..."
sleep 2
echo -e "Starting Distribution & Package Updates..."
sleep 1
echo -e "Checking for package lock (/var/lib/dpkg/lock-frontend)..."
echo -e "if this doesn't clear, quit the script and resolve lock issues"
while sudo lsof /var/lib/dpkg/lock-frontend; do sleep 10; done;
echo -e "dpkg not locked, starting dist-upgrade..."
sleep 1
sudo apt-get dist-upgrade -y
echo -e "Distribution upgrade finished..."
sleep 1
sudo apt-get upgrade -y
echo -e "Packages upgrade finished..."
sleep 1
sudo apt-get autoremove -y
echo -e "Redundant packages removed..."
sleep 1
sudo apt-get autoclean -y
echo -e "Local repository cleaned..."
sleep 1

days () { uptime | awk '/days?/ {print $3; next}; {print 0}'; }
UPTIME_THRESHOLD=$rebootUptime

if [ $(days) -ge $UPTIME_THRESHOLD ]; then
    echo -e "Uptime is more than ${UPTIME_THRESHOLD} days, rebooting..."
    sudo reboot now
elif [ -f /var/run/reboot-required ]; then
    echo -e "Reboot required! Stopping services and rebooting..."
    sudo reboot now
else
    echo -e "$Update Finished. No Reboot Required."
fi
