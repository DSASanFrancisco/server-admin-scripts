#!/bin/bash

cd vaultwarden

echo "Starting vaultwarden upgrade"
docker-compose pull
echo "Stopping vaultwarden container"
docker-compose stop
echo "Removing vaultwarden container"
docker-compose rm
echo "Start new vaultwarden container"
docker-compose up -d

echo "Done!"
